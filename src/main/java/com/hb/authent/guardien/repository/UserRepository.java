package com.hb.authent.guardien.repository;

import com.hb.authent.guardien.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
        User findByUsername(String username);
}
