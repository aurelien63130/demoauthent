package com.hb.authent.guardien.controller;

import com.hb.authent.guardien.model.User;
import com.hb.authent.guardien.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import javax.validation.Valid;

@Controller
public class RegisterController {

    @Autowired
    private UserService userService;

    @GetMapping("/register")
    public ModelAndView sayHello() {
        ModelAndView mv = new ModelAndView("security/register");
        User user = new User();

        mv.addObject("user", user);

        return mv;
    }

    @PostMapping("/register")
    public ModelAndView processRegister(@Valid User user, BindingResult br){

        if(br.hasErrors()){
            ModelAndView mv = new ModelAndView("security/register");
            mv.addObject("user", user);
            return mv;
        } else {
            this.userService.registerUser(user.getUsername(), user.getPassword());

            return new ModelAndView("redirect:/login");
        }

    }
}
