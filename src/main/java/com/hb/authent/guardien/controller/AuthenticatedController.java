package com.hb.authent.guardien.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AuthenticatedController {
    @GetMapping("/")
    public ModelAndView sayHello() {
        ModelAndView mv = new ModelAndView("authenticated");

        return mv;
    }


    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login() {
        ModelAndView mv = new ModelAndView("security/login");
        return  mv;
    }
}
